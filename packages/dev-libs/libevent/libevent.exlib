# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libevent-1.4.3.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

MY_PNV="${PNV}-stable"

require github [ user=libevent suffix=tar.gz release=release-${PV}-stable ]
require alternatives

export_exlib_phases src_test src_install

SUMMARY="A library to execute a function when a specific event occurs on a file descriptor"
HOMEPAGE+=" https://${PN}.org"

REMOTE_IDS+=" freecode:${PN}"

LICENCES="BSD-3"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        !dev-libs/libevent:2.1[>=2.1.11] [[
            description = [ Faulty version bump landed in the wrong SLOT ]
            resolution = uninstall-blocked-after
        ]]
    run:
        !dev-libs/libevent:0[<2.0.22-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
        !dev-libs/libevent:2.1[<2.1.10] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

if ever at_least 2.1.8 ; then
    DEPENDENCIES+="
        build:
            virtual/pkg-config
    "

    DEFAULT_SRC_CONFIGURE_PARAMS+=( --disable-samples )
fi

WORK="${WORKBASE}"/${MY_PNV}

DEFAULT_SRC_PREPARE_PATCHES+=( "${FILES}"/${PN}-2.0.10-test.patch )

DEFAULT_SRC_CONFIGURE_PARAMS+=(
    --enable-openssl
    --disable-static
)

libevent_src_test() {
    pushd test
    edo ./test.sh > "${TEMP}"/tests
    cat "${TEMP}"/tests
    grep -q FAILED "${TEMP}"/tests && die "1 or more tests failed"
    popd
}

libevent_src_install() {
    local alternatives=()
    local host=$(exhost --target)

    default

    alternatives+=(
        /usr/${host}/bin/event_rpcgen.py   event_rpcgen-${SLOT}.py
        /usr/${host}/include/event2        event2-${SLOT}
        /usr/${host}/include/evdns.h       evdns-${SLOT}.h
        /usr/${host}/include/event.h       event-${SLOT}.h
        /usr/${host}/include/evhttp.h      evhttp-${SLOT}.h
        /usr/${host}/include/evrpc.h       evrpc-${SLOT}.h
        /usr/${host}/include/evutil.h      evutil-${SLOT}.h
        /usr/${host}/lib/${PN}.la          ${PN}-${SLOT}.la
        /usr/${host}/lib/${PN}.so          ${PN}-${SLOT}.so
        /usr/${host}/lib/${PN}_core.la     ${PN}_core-${SLOT}.la
        /usr/${host}/lib/${PN}_core.so     ${PN}_core-${SLOT}.so
        /usr/${host}/lib/${PN}_extra.la    ${PN}_extra-${SLOT}.la
        /usr/${host}/lib/${PN}_extra.so    ${PN}_extra-${SLOT}.so
        /usr/${host}/lib/${PN}_openssl.la  ${PN}_openssl-${SLOT}.la
        /usr/${host}/lib/${PN}_openssl.so  ${PN}_openssl-${SLOT}.so
        /usr/${host}/lib/${PN}_pthreads.la ${PN}_pthreads-${SLOT}.la
        /usr/${host}/lib/${PN}_pthreads.so ${PN}_pthreads-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc          ${PN}-${SLOT}.pc
        /usr/${host}/lib/pkgconfig/${PN}_openssl.pc  ${PN}_openssl-${SLOT}.pc
        /usr/${host}/lib/pkgconfig/${PN}_pthreads.pc ${PN}_pthreads-${SLOT}.pc
    )

    if ever at_least 2.1.8 ; then
        alternatives+=(
            /usr/${host}/lib/pkgconfig/${PN}_core.pc  ${PN}_core-${SLOT}.pc
            /usr/${host}/lib/pkgconfig/${PN}_extra.pc ${PN}_extra-${SLOT}.pc
        )
    fi

    alternatives_for _${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}

