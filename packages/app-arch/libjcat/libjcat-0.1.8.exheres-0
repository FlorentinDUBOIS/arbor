# Copyright 2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=hughsie ] \
    meson \
    vala [ vala_dep=true with_opt=true option_name=gobject-introspection ]

SUMMARY="Library for reading and writing gzip-compressed JSON catalog files"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    pkcs7 [[ description = [ Support for PKCS7 verification ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        app-crypt/gpgme
        core/json-glib[>=1.1.1]
        dev-libs/glib:2[>=2.45.8]
        dev-libs/libgpg-error
        pkcs7? ( dev-libs/gnutls[>=3.6.0] )
    test:
        dev-libs/gnutls [[ note = [ certtool ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgpg=true
    -Dman=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    'gtk-doc gtkdoc'
    pkcs7
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

