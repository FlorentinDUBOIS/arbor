# Copyright 2018-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=commonmark ] cmake
require alternatives

SUMMARY="CommonMark parsing and rendering library and program in C"

LICENCES="BSD-2 MIT"
# SOVERSION ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}
SLOT="${PV}"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+test:
        dev-lang/python:*[>=3]
    run:
        !app-text/cmark:0[<0.29.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMARK_LIB_FUZZER:BOOL=FALSE
    -DCMARK_SHARED:BOOL=TRUE
    -DCMARK_STATIC:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DCMARK_TESTS:BOOL=TRUE -DCMARK_TESTS:BOOL=FALSE'
)

src_install() {
    local arch_dependent_alternatives=() other_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/bin/${PN}                  ${PN}-${SLOT}
        /usr/${host}/include/${PN}.h            ${PN}-${SLOT}.h
        /usr/${host}/include/${PN}_export.h     ${PN}_export-${SLOT}.h
        /usr/${host}/include/${PN}_version.h    ${PN}_version-${SLOT}.h
        /usr/${host}/lib/lib${PN}.so            lib${PN}-${SLOT}.so
        /usr/${host}/lib/cmake/${PN}            ${PN}-${SLOT}
        /usr/${host}/lib/pkgconfig/lib${PN}.pc  lib${PN}-${SLOT}.pc
    )

    other_alternatives+=(
        /usr/share/man/man1/${PN}.1 ${PN}-${SLOT}.1
        /usr/share/man/man3/${PN}.3 ${PN}-${SLOT}.3
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${other_alternatives[@]}"
}

