# Copyright 2009 Fernando J. Pereda
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'cyrus-sasl-2.1.22-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require flag-o-matic pam systemd-service 66-service

export_exlib_phases src_install

SUMMARY="Library implementing the Simple Authentication and Security Layer"
DESCRIPTION="
SASL is the Simple Authentication and Security Layer, a method for adding
authentication support to connection-based protocols. To use SASL, a protocol
includes a command for identifying and authenticating a user to a server and
for optionally negotiating protection of subsequent protocol interactions. If
its use is negotiated, a security layer is inserted between the protocol
and the connection
"
HOMEPAGE="https://www.cyrusimap.org"
DOWNLOADS="${HOMEPAGE}/releases/${PNV/-rc/rc}.tar.gz"

LICENCES="cyrus-sasl"
SLOT="0"
MYOPTIONS="
    doc
    kerberos
    ldap
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( libc: musl )
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/gdbm
        sys-libs/pam
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        ldap? ( net-directory/openldap )
        !libc:musl? ( dev-libs/libxcrypt:= )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"
DEFAULT_SRC_CONFIGURE_PARAMS=(
        CC_FOR_BUILD=$(exhost --build)-cc
        --enable-auth-sasldb
        --enable-login
        --enable-ntlm
        --enable-obsolete_cram_attr
        --enable-obsolete_digest_attr
        --disable-httpform
        --disable-java
        --disable-krb4
        --disable-ntlm
        --disable-otp
        --disable-sia
        --disable-sql
        --disable-srp
        --disable-static
        --with-configdir=/etc/sasl2
        --with-dblib=gdbm
        --with-dbpath=/etc/sasl2/sasldb2
        --with-des
        --with-openssl
        --with-pam
        --with-plugindir=/usr/$(exhost --target)/lib/sasl2
        --with-pwcheck=/var/lib/sasl2
        --with-saslauthd=/run/saslauthd
        --without-sphinx-build
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'kerberos gssapi'
    'ldap ldapdb'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    ldap
)

DEFAULT_SRC_INSTALL_EXTRA_SUBDIRS=( saslauthd )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( LDAP_SASLAUTHD )

cyrus-sasl_src_install() {
    default

    keepdir /var/lib/sasl2 /etc/sasl2

    newdoc pwcheck/README README.pwcheck

    if option doc ; then
        dodoc -r doc/html
    fi

    pamd_mimic_system saslauthd auth auth account session

    install_66_files

    # systemd
    install_systemd_files
    insinto /etc/conf.d
    newins "${FILES}"/saslauthd-confd saslauthd.conf
    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins saslauthd.conf <<EOF
d /run/saslauthd 0750 root root
EOF
}

